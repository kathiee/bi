function [nombre_grafico] = grafico (data, k,nombre_grafico)
  more off;
  %data = load("TSerie.txt");
  %index = [1 : 588]';
  %dataset = [index data];
  for i = 1:k
    autocorr(i) = autocorrelacion(data, i);% calcula la autocorrelacion 
  end
  alpha=0.05;
  %Calculo IC
  [ConfidenceInterval, N]= intervaloDeConfianza (autocorr)
  %Grafico de autocorrelacion
  figure(1);
  h = stem(autocorr);
  hold on
  line ([0 N], [ConfidenceInterval(1)  ConfidenceInterval(1)], "linestyle", "--", "color", "r");
  line ([0 N], [ConfidenceInterval(2)  ConfidenceInterval(2)], "linestyle", "--", "color", "r");
  hold off
  xlabel('Lag');
  ylabel('Autocorrelacion');
  title("Autocorrelacion vs Lag");
  grid on
  print -dpng ACF 
endfunction
