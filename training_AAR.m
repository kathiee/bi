## Copyright (C) 2018 Kyare
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} training_AAR (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn



function [sol] = training_AAR (sol, data)

  [y_L a_l]=AR (sol.x_L , data,sol);
  sol.x_H_train=x_H;
  sol.x_L_train=x_L;
  [y_H a_h]=AR (x_H , data,sol);
  y_pred=y_L+y_H;
  sol.y_pred_train=y_pred';
  sol.a_h=a_h;
  sol.a_l=a_l;
endfunction
