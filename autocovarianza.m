function [autocovar] = autocovarianza (data,k)
   N = rows(data);
   
   media = mean(data);
   factor = 1/(N-k);
   sum=0;
   for t=1:(N-k)
     sum=sum+((data(t)-media)*(data(t+k)-media));
   end
   autocovar = factor*sum;
      
endfunction
