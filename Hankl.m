## Copyright (C) 2018 Kyare
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} Hank (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn



function [H] = Hankl (data, L)
N=rows(data); %largo 
M=N-L+1; %columnas
K=M;
if (L>N)
  display("Error: L es mayor que la cantidad de datos");
endif

for i=1:L
  for j=1:M
    H(i, j)=data(i+j-1);
  endfor  
endfor
endfunction
