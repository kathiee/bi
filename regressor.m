## Copyright (C) 2018 Kyare
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} regressor (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Kyare <Kyare@KATHIE>
## Created: 2018-07-04

function [x, y] = regressor (data,h,lag)
  
  len=length(data)-lag-h+1;
  for i=1:len
    xReg(i,:)= data(i:i+lag+h-1)';
  endfor
  
  x=xReg(:,1:lag);
  y=xReg(:,end);
endfunction
