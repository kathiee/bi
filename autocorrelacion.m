function [autocorr] = autocorrelacion (data, k)
  cov_k=autocovarianza(data,k);

  cov_cero=autocovarianza(data,0);

  autocorr=cov_k/cov_cero;
endfunction
